#pragma once
#include <stdint.h>
#include <tuple>
#include <array>
#include <functional>
class constexpr_sha1
{
private:
	//due to std::array<>::operator[] not being marked constexpr we have to hack around it with c arrays; this allows us to easily convert a c array to a std::array<>
	template<typename type, size_t N, size_t... inds>
	constexpr static std::array<type, N> c_array_to_std_impl(type(&data)[N], std::index_sequence<inds...>)
	{
		return std::array<type, N>{ {data[inds]...}};
	}
	template<typename type, size_t N>
	constexpr static std::array<type, N> c_array_to_std(type(&data)[N])
	{
		return c_array_to_std_impl(data, std::make_index_sequence<N>{});
	}
	template<size_t buffer_size>
	struct buffer
	{
		std::array<uint8_t, buffer_size> buffer_;
		size_t pos;

		constexpr static size_t min_uint32_t_size = buffer_size / sizeof(uint32_t);
		static_assert(min_uint32_t_size * sizeof(uint32_t) == buffer_size, "Buffer size must be a multiple of 4");

		template<typename itr>
		constexpr itr add(itr begin, const itr end)
		{
			for (size_t ind = pos; ind < buffer_size && begin != end; ++ind, ++begin)
			{
				buffer_[ind] = *begin;
				++pos;
			}
			return begin;
		}
		constexpr void fill()
		{
			if (pos == buffer_size - 1)
				return;
			buffer_[pos] = 0x80;
			pos = buffer_size - 1;
		}
		constexpr operator std::array<uint32_t, min_uint32_t_size>() const
		{
			std::array<uint32_t, min_uint32_t_size> arr{};
			for (size_t i = 0; i < min_uint32_t_size; ++i)
			{
				arr[i] = buffer_[4 * i + 3]
					| buffer_[4 * i + 2] << 8
					| buffer_[4 * i + 1] << 16
					| buffer_[4 * i] << 24;
			}
			return arr;
		}
	};
public:
	//the working/resulting digest of calling sha1.
	//can be compared/sorted or converted into a buffer of 40 chars
	struct digest
	{
		using digest_type = uint32_t;
		static constexpr size_t data_size = 5;
		std::array<uint32_t, 5> data;
		constexpr static digest initial_digest()
		{
			//sha1 intialization constants
			return digest{ {
					0x67452301,
					0xefcdab89,
					0x98badcfe,
					0x10325476,
					0xc3d2e1f0 } };
		}
		constexpr void shifted_digest_right()
		{
			data = { data[4], data[0], data[1], data[2], data[3] };
		}
		constexpr static char char_to_hex(char num)
		{
			if (num < 10)
				return '0' + num;
			return 'a' + (num - 10);
		}
		constexpr static uint32_t hex_to_char(char hex)
		{
			if (hex <= '9')
				return hex - '0';
			if (hex <= 'f' && hex >= 'a')
				return hex - 'a' + 10;
			return hex - 'A' + 10;
		}
		constexpr decltype(auto) hexstring() const
		{
			constexpr size_t char_count = sizeof(uint32_t) * 2;	//due to hex only eating 4 bits out of our 8 for a byte.
			std::array<char, data_size*char_count> hexstr{};
			for (size_t curr_num = 0; curr_num < data_size; ++curr_num)
			{
				for (size_t shift_amount = 0; shift_amount < char_count; ++shift_amount)
				{
					//we need to slot the higher order bits in first. aka [0] = 0xF--- ---- , [1] = 0x-F-- ---- etc.
					hexstr[shift_amount + curr_num * char_count] = char_to_hex((data[curr_num] >> (char_count - 1 - shift_amount) * 4) & 0xf);
				}
			}
			return hexstr;
		}
		constexpr static digest from_hexstring(const char(&digest_str)[41])
		{
			digest _digest{};
			for (size_t ind = 0; ind < 5; ++ind)
			{
				//shifting around our hex chars into appropriate format for a uint32_t.
				//we consume 8 chars at a time from a string 0xa999
				//_digest.data[ind >>3] |= hex_to_char(digest_str[ind]) << ((char_count - ind & (char_count -1)) * 4 - 4);
				_digest.data[ind] =
					hex_to_char(digest_str[8 * ind + 0]) << 28
					| hex_to_char(digest_str[8 * ind + 1]) << 24
					| hex_to_char(digest_str[8 * ind + 2]) << 20
					| hex_to_char(digest_str[8 * ind + 3]) << 16
					| hex_to_char(digest_str[8 * ind + 4]) << 12
					| hex_to_char(digest_str[8 * ind + 5]) << 8
					| hex_to_char(digest_str[8 * ind + 6]) << 4
					| hex_to_char(digest_str[8 * ind + 7]) << 0;
			}
			return _digest;
		}
		constexpr operator std::array<uint32_t, 5>() const
		{
			return { { data[0],data[1],data[2],data[3],data[4] } };
		}
		constexpr uint32_t& operator[](const size_t ind)
		{
			return data[ind];
		}
		constexpr bool operator==(const digest& right) const
		{
			for (size_t i = 0; i < 5; ++i)
			{
				if (data[i] != right.data[i])
					return false;
			}
			return true;
		}
		constexpr bool operator==(const char(&digest_str)[41]) const
		{
			return *this == from_hexstring(digest_str);
		}
		constexpr bool operator!=(const digest& right) const
		{
			return !(*this == right);
		}
		constexpr bool operator<(const digest& right) const
		{
			return std::make_tuple(data[0], data[1], data[2], data[3], data[4]) < std::make_tuple(right.data[0], right.data[1], right.data[2], right.data[3], right.data[4]);
		}
	};
private:
	constexpr static size_t block_ints = 16;
	constexpr static size_t block_bytes = block_ints * sizeof(int);
	using data_block = std::array<uint32_t, block_ints>;

	struct data_and_digest
	{
		data_block data;
		digest digest_;
	};
	struct transform_packet
	{
		digest _digest;
		uint64_t transform_count;
	};

	using bbuffer = buffer<block_bytes>;


	constexpr static data_block assign_slot(const data_block arr, size_t ind, const uint32_t val)
	{
		uint32_t tmp[block_ints]{};
		for (size_t i = 0; i < block_ints; ++i)
		{
			if (i != ind)
				tmp[i] = arr[i];
			else
				tmp[i] = val;
		}
		return c_array_to_std(tmp);
	}
	constexpr static uint32_t rol(uint32_t value, uint32_t bits)
	{
		return (value << bits) | (value >> (32 - bits));
	}
	constexpr static uint32_t blk(data_block block, uint32_t i)
	{
		return rol(block[(i + 13) & 15] ^ block[(i + 8) & 15] ^ block[(i + 2) & 15] ^ block[i], 1);
	}


	//(R0+R1), R2, R3, R4 are the different operations used in SHA1
	constexpr static void R0(data_block& block, uint32_t v, uint32_t &w, uint32_t x, uint32_t y, uint32_t &z, uint32_t i)
	{
		z = z + ((w&(x^y)) ^ y) + block[i] + 0x5a827999 + rol(v, 5);
		w = rol(w, 30);
	}
	constexpr static void R1(data_block& block, uint32_t v, uint32_t &w, uint32_t x, uint32_t y, uint32_t &z, uint32_t i)
	{
		block[i] = blk(block, i);
		z = z + ((w&(x^y)) ^ y) + block[i] + 0x5a827999 + rol(v, 5);
		w = rol(w, 30);
	}
	constexpr static void R2(data_block& block, uint32_t v, uint32_t &w, uint32_t x, uint32_t y, uint32_t &z, uint32_t i)
	{
		block[i] = blk(block, i);
		z = z + (w^x^y) + block[i] + 0x6ed9eba1 + rol(v, 5);
		w = rol(w, 30);
	}
	constexpr static void R3(data_block& block, uint32_t v, uint32_t &w, uint32_t x, uint32_t y, uint32_t &z, uint32_t i)
	{
		block[i] = blk(block, i);
		z = z + (((w | x)&y) | (w&x)) + block[i] + 0x8f1bbcdc + rol(v, 5);
		w = rol(w, 30);
	}
	constexpr static void R4(data_block& block, uint32_t v, uint32_t &w, uint32_t x, uint32_t y, uint32_t &z, uint32_t i)
	{
		block[i] = blk(block, i);
		z = z + (w^x^y) + block[i] + 0xca62c1d6 + rol(v, 5);
		w = rol(w, 30);
	}
	//hash a single 512-bit block. This is the core of the algoirthm
	constexpr void transform(digest& digest_, data_block block, uint64_t& transform_count)
	{
		uint32_t a = digest_[0];
		uint32_t b = digest_[1];
		uint32_t c = digest_[2];
		uint32_t d = digest_[3];
		uint32_t e = digest_[4];
		uint32_t i = 0;
		// 4 rounds of 20 operations each. Loop unrolled. 
		R0(block, a, b, c, d, e, 0);
		R0(block, e, a, b, c, d, 1);
		R0(block, d, e, a, b, c, 2);
		R0(block, c, d, e, a, b, 3);
		R0(block, b, c, d, e, a, 4);
		R0(block, a, b, c, d, e, 5);
		R0(block, e, a, b, c, d, 6);
		R0(block, d, e, a, b, c, 7);
		R0(block, c, d, e, a, b, 8);
		R0(block, b, c, d, e, a, 9);
		R0(block, a, b, c, d, e, 10);
		R0(block, e, a, b, c, d, 11);
		R0(block, d, e, a, b, c, 12);
		R0(block, c, d, e, a, b, 13);
		R0(block, b, c, d, e, a, 14);
		R0(block, a, b, c, d, e, 15);
		R1(block, e, a, b, c, d, 0);
		R1(block, d, e, a, b, c, 1);
		R1(block, c, d, e, a, b, 2);
		R1(block, b, c, d, e, a, 3);
		R2(block, a, b, c, d, e, 4);
		R2(block, e, a, b, c, d, 5);
		R2(block, d, e, a, b, c, 6);
		R2(block, c, d, e, a, b, 7);
		R2(block, b, c, d, e, a, 8);
		R2(block, a, b, c, d, e, 9);
		R2(block, e, a, b, c, d, 10);
		R2(block, d, e, a, b, c, 11);
		R2(block, c, d, e, a, b, 12);
		R2(block, b, c, d, e, a, 13);
		R2(block, a, b, c, d, e, 14);
		R2(block, e, a, b, c, d, 15);
		R2(block, d, e, a, b, c, 0);
		R2(block, c, d, e, a, b, 1);
		R2(block, b, c, d, e, a, 2);
		R2(block, a, b, c, d, e, 3);
		R2(block, e, a, b, c, d, 4);
		R2(block, d, e, a, b, c, 5);
		R2(block, c, d, e, a, b, 6);
		R2(block, b, c, d, e, a, 7);
		R3(block, a, b, c, d, e, 8);
		R3(block, e, a, b, c, d, 9);
		R3(block, d, e, a, b, c, 10);
		R3(block, c, d, e, a, b, 11);
		R3(block, b, c, d, e, a, 12);
		R3(block, a, b, c, d, e, 13);
		R3(block, e, a, b, c, d, 14);
		R3(block, d, e, a, b, c, 15);
		R3(block, c, d, e, a, b, 0);
		R3(block, b, c, d, e, a, 1);
		R3(block, a, b, c, d, e, 2);
		R3(block, e, a, b, c, d, 3);
		R3(block, d, e, a, b, c, 4);
		R3(block, c, d, e, a, b, 5);
		R3(block, b, c, d, e, a, 6);
		R3(block, a, b, c, d, e, 7);
		R3(block, e, a, b, c, d, 8);
		R3(block, d, e, a, b, c, 9);
		R3(block, c, d, e, a, b, 10);
		R3(block, b, c, d, e, a, 11);
		R4(block, a, b, c, d, e, 12);
		R4(block, e, a, b, c, d, 13);
		R4(block, d, e, a, b, c, 14);
		R4(block, c, d, e, a, b, 15);
		R4(block, b, c, d, e, a, 0);
		R4(block, a, b, c, d, e, 1);
		R4(block, e, a, b, c, d, 2);
		R4(block, d, e, a, b, c, 3);
		R4(block, c, d, e, a, b, 4);
		R4(block, b, c, d, e, a, 5);
		R4(block, a, b, c, d, e, 6);
		R4(block, e, a, b, c, d, 7);
		R4(block, d, e, a, b, c, 8);
		R4(block, c, d, e, a, b, 9);
		R4(block, b, c, d, e, a, 10);
		R4(block, a, b, c, d, e, 11);
		R4(block, e, a, b, c, d, 12);
		R4(block, d, e, a, b, c, 13);
		R4(block, c, d, e, a, b, 14);
		R4(block, b, c, d, e, a, 15);

		digest_[0] = digest_[0] + a;
		digest_[1] = digest_[1] + b;
		digest_[2] = digest_[2] + c;
		digest_[3] = digest_[3] + d;
		digest_[4] = digest_[4] + e;
		transform_count = transform_count + 1;	//for some reason visual studio chokes on _any_ += equivalent.
	}

public:

	transform_packet state;
	bbuffer byte_buffer;

	constexpr static constexpr_sha1 init()
	{
		return constexpr_sha1{ transform_packet{ digest::initial_digest(),0 }, bbuffer{} };
	}
	template<typename itr, std::enable_if_t<std::is_same<typename std::iterator_traits<itr>::value_type, char>::value, bool> = true>
	constexpr void update(itr begin, itr end)
	{
		auto itr = byte_buffer.add(begin, end);
		do
		{
			if (byte_buffer.pos != block_bytes)
				break;
			transform(state._digest, byte_buffer, state.transform_count);
			byte_buffer = {};
			itr = byte_buffer.add(itr, end);

		} while (itr != end);
	}
	//this is only here so we can do a hack with visual studio not allowing proper fold expressions, nor comma operators with a void type.. aka poor mans fold expressions
	template<typename itr, std::enable_if_t<std::is_same<typename std::iterator_traits<itr>::value_type, char>::value, bool> = true>
	constexpr constexpr_sha1& update_and_ref(itr begin, itr end)
	{
		update(begin, end);
		return *this;
	}
	template<size_t size>
	constexpr void update(const char(&str)[size])
	{
		update(&str[0], &str[size - 1]);	//remove the trailing '/0'
	}
	constexpr digest final()
	{
		std::uint64_t transform_count_ignored = 0;
		uint64_t total_bits = (state.transform_count*block_bytes + byte_buffer.pos) * 8;
		std::uint64_t curr_buffer_pos = byte_buffer.pos;
		byte_buffer.fill();
		if (curr_buffer_pos > block_bytes - 8)
		{
			//the transform count is ignored now; so no reason to pass it.
			transform(state._digest, byte_buffer, transform_count_ignored);

			byte_buffer = {};
		}
		std::array<uint32_t, block_ints> final_block = byte_buffer;
		//final_block[block_ints - 1] = total_bits; //can't do this due to std::array<>::operator[] not being valid in constexpr statements. oh well. we've got a work around function.
		final_block[block_ints - 1] = static_cast<uint32_t>(total_bits);
		final_block[block_ints - 2] = static_cast<uint32_t>(total_bits >> 32);

		transform(state._digest, final_block, transform_count_ignored);
		return state._digest;
	}
	template<typename itr, std::enable_if_t<std::is_same<typename std::iterator_traits<itr>::value_type, char>::value, bool> = true>
	constexpr static digest get_checksum(itr begin, itr end)
	{
		constexpr_sha1 checksum = constexpr_sha1::init();
		checksum.update(begin, end);
		return checksum.final();
	}
	template<size_t size>
	constexpr static digest get_checksum(const char(&str)[size])
	{
		return get_checksum(&str[0], &str[size - 1]);	//remove the trailing '/0'
	}
};
namespace std
{
	template<>
	struct hash<constexpr_sha1::digest>
	{
		using type = constexpr_sha1::digest;
		using result_type = std::uint64_t;
		result_type operator()(const type& in) const noexcept
		{
			//assuming that sha1 is actually even on all bits this should be sufficient (take the last 2 ints as a 64bit hash)
			return (static_cast<result_type>(in.data[3]) << 32ull) | in.data[4];
		}
	};
}