#include "constexpr_sha1.h"

constexpr decltype(auto) looped_constexpr_check()
{
	//VS2017 was having issues with constexpr user defined constructors but somehow an initiazation funtion works fine.
	constexpr_sha1 checksum = constexpr_sha1::init();

	const char str[] = "a string that's input twice";
	auto begin = &str[0];
	auto end = &str[sizeof(str) / sizeof(char) - 1];

	//if the user decides to they can update in periodic chunks with both an itr and a standard str array. (given the array size is known as part of the type system).
	checksum.update(begin, end);
	checksum.update(str);
	//return the final digest.
	return checksum.final();
}
int main()
{
	constexpr auto digest = constexpr_sha1::get_checksum("abc");	//for some reason intellisense does not generate the right preview digest.
	//intellisense has a tendency of choking on this and as of 5/11/2017 says the assert should fail.
	static_assert(digest == "a9993e364706816aba3e25717850c26c9cd0d89d", "Expected sha1 checksum missmatch");

	static_assert(constexpr_sha1::get_checksum("constexpr_sha1") == "2f4068ee43079eb279719849b001a427c39795e1", "Sha1 missmatch");

	//showing that we can also use standard iterators (so long as their value_type is char)
	//static because some compilers have issues detecting compile time constexpr strings otherwise.
	constexpr static const char str[] = "Some kind of string that's longer than the default digest size of 64 bytes/512 bits";
	constexpr auto begin = &str[0];
	constexpr auto end = &str[sizeof(str) / sizeof(char) - 1];	//remove the null character.
	
	constexpr_sha1::get_checksum(begin, end) == "0a2c0fc0f33031ffa372b2e3f3276b7bce1b7c70";
	//static_assert(looped_constexpr_check() == "d9b892876b9ba5a13cde055eaac6e733bc9d68e1", "Sha1 missmatch");
}